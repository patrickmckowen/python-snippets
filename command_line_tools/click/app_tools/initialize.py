import click
import os

@click.command()
@click.option('--framework', default='Sinatra', help='Number of greetings.')
@click.option('--app_name', prompt='What is the name of the app?', help='This is used to name the main folder.')
def init(framework, app_name):
    """ """
    if app_name not in os.listdir():
        os.mkdir(app_name)
        os.chdir(app_name)
        os.mkdir('views')
        os.chdir('views')
        
        # CREATE : LAYOUT.RB
        with open('layout.erb', 'w') as file:
            file.write('''
<head>
	<link href="css/style.css" rel="stylesheet">
</head>
<body>

	<div>
		<ul>
			<li><a href="/" target="_blank">Home</a></li>
			<li><a href="/calculator">Calculator</a></li>
			<li><a href="/coin_changer">Coin Changer</a></li>
			<li><a href="http://www.minedminds.org" target="_blank">Mined Minds</a></li>
		</ul>
	</div>
    <div class="main">

        <%=yield%>

    </main>
    
    <footer>
        <a href="mined_minds.org">Mined Minds</a>
    </footer>

</body>
                          ''')

        # CREATE: INDEX.RB  
        with open('index.erb', 'w') as file:
            file.write("<!-- Index.erb --->")
        os.chdir(r"../")

        # CREATE DIRECTORY: PUBLIC
        os.mkdir('css')
        
        os.mkdir('public')

        # ->/PUBLIC
        os.chdir('public')

        # *PUBLIC/CSS
        os.mkdir('css')

        # *PUBLIC/JS
        os.mkdir('js')

        # *PUBLIC/IMAGES
        os.mkdir('images')
        
    
if __name__ == '__main__':
    init()
