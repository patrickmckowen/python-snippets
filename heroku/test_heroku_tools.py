import os

import heroku
import ruby


def test_internal():
	'''
	This test checks making functions as well as wether the assert function needs to be the last line of the function.

	- Having a line afterward allows for deleting temporary files and directories after checking to make sure they are working.
	'''
	os.mkdir("test_dir")
	assert "test_dir" in os.listdir(os.getcwd())
	os.rmdir("test_dir")

def 