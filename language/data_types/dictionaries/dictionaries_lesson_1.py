"""
Created on Fri Jul  7 22:32:10 2017

@author: Patrick

Using lists are handy but what if we don't know the index position
of the item or items we want to retrieve?

Imagine if a real dictionary required you look up words by index!
Even if the publishers were considerate enough to put the words in
alphebetical order, without the word itself to know what you are looking
up, you wouldn't even know when you found the definition.

This is why dictionaries include the words themselves and also why programming
laguages usually include a multi-item data structure which relies on retrieving
values by specifying a 'key'.

In python this is appropriately called a dictionary but other languages may
call it a hash.

Let's try creating a dictionary, inserting and retrieving data, and playing around
with some of Python's handy built-in methods for this data structure.

clear - Removes everything
copy -
fromkeys
popitem
update
"""

# Our company wants an easier way to look up the manufacturing
# cost of various models so they write this.

manufacturing_costs = {'Lumina': 15000, 'Cobalt': 12347, 'Camaro': 23412}

# Let's access and print the cost for the venerable Lumina
print("Lumina's cost of manufacturing:", manufacturing_costs['Lumina'])

# Now how about the Cobalt
print("Cobalt's cost of manufacturing:", manufacturing_costs['Cobalt'])

# The company now needs to insert a new vehicle.
#  - This simply involves specifying a key that is not in the dict already and then using the =
#    to give the value
manufacturing_costs['Grand Am'] = 14237

# Let's check if we successfully inserted the new car
print("Grand Am's cost of manufacturing:", manufacturing_costs['Grand Am'])

# Get just the keys
print("manufacturing_costs.keys() -->", manufacturing_costs.keys())

# Get just the values
print("manufacturing_costs.values() -->", manufacturing_costs.values())

# Get just the "items"
print("manufacturing_costs.items() -->", manufacturing_costs.items())

# The handy .get() function takes the 1. the item and 2. a message to display
# if it is not in the dictionary. (Can be used as an error message or a useful return value.)
print(manufacturing_costs.get('Lumina', 'The vehicle is not in the dictionary.'))

# Now we'll test the .get() function on a non-existant item
print(manufacturing_costs.get('F150', 'The vehicle is not in the dictionary.'))

# The .setdefault() method is similar to the get but will create the item on the spot
# and you will get that value returned for that call plus it will be available in the
# future as it is now part of the data structure
print(manufacturing_costs.setdefault('Camry', 12000))

# Check it out. Now when we print the data structure, we see the new items.
print("Manufacturing costs after using .setdefault() -->", manufacturing_costs)

# Calling the .pop() function and giving it a key will return the value and remove the key:value pair from the dictionary.
# Also, like .get(), we can choose to specify a return value if the item is not present
# Since we have the iem int the data structure we will get it's value...
print("Using the .pop() function for an item in the dictionary:", manufacturing_costs.pop('Cobalt', "The item is not in the dictionary."))

# Since the function removes the item, calling the exact same function this time will return the default message.
print("Using the .pop() function a second time:", manufacturing_costs.pop('Cobalt', "The item is not in the dictionary."))

# And in case you were wondering, yes, we can add dictionaries inside of dictionaries
overall_costs = {'Machinery Costs': {'mill': 20334, 'lathe': 21578}}

#
overall_costs['Manufacturing Costs'] = manufacturing_costs

# What's it look like?
print(overall_costs)

# Let's get to some of that data
print("overall_costs['Manufacturing Costs']['Lumina'] -->", overall_costs['Manufacturing Costs']['Lumina'])

# What if we wanted to change data - it's just the same as when we assigned it.
overall_costs['Manufacturing Costs']['Lumina'] = 100000

# Checking
print("overall_costs['Manufacturing Costs']['Lumina'] -->", overall_costs['Manufacturing Costs']['Lumina'])

#
a={1:2,3:4}
b={5:6,7:8}
c={**a, **b}
print(c)
