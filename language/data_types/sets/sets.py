'''

One thing to consider is that sets are un ordered, so if I created a set
and called print(the_set) twice, it would most likely print the set in
different order (although it could be the same by chance).

add() - 
clear() - Clears all items from the set.
copy() -
difference() -
difference_update() - 
discard()
intersection()
intersection_update()
isdisjoint()
issubset()
issuperset()
pop()
remove()
symmetric_difference()
symmetric_difference_update()
union()
update()
'''
#
cars = ['Lumina', 'Cobalt', 'Lumina', 'Camaro', 'Grand Am','Ultima', 'Cobalt', 'F150']

duplicates_removed = set(cars)

print(duplicates_removed)


cars2 = {'Lumina', 'Cobalt', 'Lumina', 'Camaro', 'Grand Am','Ultima', 'Cobalt', 'F150', 'Ultima'}

print(cars2)

# Trans Am is not already in the set so we can add it to the set
cars2.add('Trans Am')
print(cars2)

# This will not insert Lumina since it's already a set. (It will not give an error though.)
cars2.add('Lumina')
print(cars2)


# ------------------------------------------------------------------------------------- #
a_string = 'A random STRING of text!'



string_into_set = set(a_string)
string_into_frozenset = frozenset(a_string)

string_into_frozenset.add('a')


print(turned_into_a_set)
