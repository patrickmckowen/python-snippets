'''
Sometimes you want to be able to take an arbitrary number of arguments but
you don't know what type of information you will be recieving.

This is where arbitrary keyword arguements come in handy.

In the function definition, you simply put two *s before the argument name
ex: def some_function(**arb_keyword_args):

Then you'll be cable to specify akeyword, then an = then a value in the argument list.

The data will be passed into the body of the function as a dictionary.
'''

def build_profile(first, last, **user_info):
    profile = {}
    profile['first_name'] = first
    profile['last_name'] = last
    for key, value in user_info.items():  # Note the technique for looping through dictionaries
        profile[key] = value
    return profile

user_profile = build_profile('albert', 'Einstein',
                             location = 'Princeton',
                             field = 'Physics')
print(user_profile)    
