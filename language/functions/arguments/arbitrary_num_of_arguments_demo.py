# We start with the most basic demonstration
# Notice there is a star in the function definition and not in the function body
def print_arbitrary_num_of_args(*args):
    print(args)

# Now let's loop through the args
def loop_through_arbitrary_num_of_args(*args):
    for i in args:
        print(i)
    
# Next we'll take a look at what data type gets is passed into the function body
# We need to convert the return of type(*args) to a string because it is not a string and we can't concat it
# with the rest of the string
def print_arbitrary_num_of_args_return_type(*args):
    print('*args type: ' + str(type(args)))

# Let's try mixing arb args and normal args
# You can specify what you want at the end of the print command. We don't want a new line
# because we want the value of 'arb_args' to come right after the text prompt
# we could't just put the value of 'arg_args' because we can't concat a string and a tuple
def arbitrary_and_normal_args(arg1, arg2, *arb_args):
    print('arg1 = ' + arg1 + "\n" + 'arg2 = ' + arg2 + "\n" + '*arb_args = ', end='') 
    print(arb_args)

# ------------------------------------------------------------------------------------------------- #
                              # Now let's call those functions! #
# Simply print args
print_arbitrary_num_of_args(1,2,3,4,5,6,7)

# Loops through *args
loop_through_arbitrary_num_of_args(1,2,3,4,5,6,7)

# Print the data type of *args
print_arbitrary_num_of_args_return_type(1,2,3,4,5,6,7)

# Try combining simple positional args and arbitrary number of args
arbitrary_and_normal_args('one', 'two', 3,4,5,6,7)
