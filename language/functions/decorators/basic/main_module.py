'''

Imports the module and uses the decorator function to decorate a function.

'''
from decorator_module import my_decorator






@my_decorator                  # Calling the decorator
def just_some_function():      # Defining the function
    print("The main module!")  # Printing a thing # Con it also pass returned data?





# Calling the function
just_some_function()
