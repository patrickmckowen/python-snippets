'''
This program takes in user input and prints it back to the screen as long as
it is not the exit message.
    - We add an extra rule to prevent the response from being printed to the screen if
     the quit message is given.
'''

prompt = "\nTell me something, and I will repeat it back to you:"
prompt += "\nEnter 'blow this popsicle stand' to end the program. "

message = ''

while message != 'blow this popsicle stand':
    message = input(prompt)

    # We add this to prevent the program from printing the quit message too
    if message != 'quit': 
        print(message)
