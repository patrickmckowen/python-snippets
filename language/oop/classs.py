class Sample(object):
	'''A test class'''
	spl_count = 0

	def __init__(self, name, song_name, artist, rating, keywords = []):
		self.spl_name = name
		self.song_name = song_name
		self.artist = artist
		self.rating = rating
		self.keywords = keywords
		Sample.spl_count += 1
   
	def get_spl_count(self):
		return Sample.spl_count

	def get_spl_name(self):
		return self.spl_name

	def get_song_name(self):
		return self.song_name

	def get_artist(self):
		return self.artist

	def get_rating(self):
		return self.rating

	def get_keywords(self):
		return self.keywords



'''
The getattr(obj, name[, default]) : to access the attribute of object.

The hasattr(obj,name) : to check if an attribute exists or not.

The setattr(obj,name,value) : to set an attribute. If attribute does not exist, then it would be created.

The delattr(obj, name) : to delete an attribute.


BUILT IN CLAS METHODS

__dict__: Dictionary containing the class's namespace.

__doc__: Class documentation string or none, if undefined.

__name__: Class name.

__module__: Module name in which the class is defined. This attribute is "__main__" in interactive mode.

__bases__: A possibly empty tuple containing the base classes, in the order of their occurrence in the base class list.



BASE OVERLOADING METHODS


SN	Method, Description & Sample Call
1	__init__ ( self [,args...] )
Constructor (with any optional arguments)
Sample Call : obj = className(args)

2	__del__( self )
Destructor, deletes an object
Sample Call : del obj

3	__repr__( self )
Evaluatable string representation
Sample Call : repr(obj)

4	__str__( self )
Printable string representation
Sample Call : str(obj)

5	__cmp__ ( self, x )
Object comparison
Sample Call : cmp(obj, x)

'''