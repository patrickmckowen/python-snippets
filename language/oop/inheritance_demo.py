'''
Things to cover

Non-init variables
overriding non-init variables

default values
overiding non-init values

Extending a parent class method in a child class
example:
    def __str__(self):
        return super().__str__() + ", " +  self.staffnumber

Extending a class in a differenct file
'''

class Battery(object):
    '''


    '''
    
    def __init__(self, size='55mw'):
        '''
        
        '''
        self.size = size

    def get_size():
        '''
        '''
        return self.size

    
class Vehicle(object):
    '''
    Inherits from 
    '''
    
    number_of_wheels = 4
    storage = "trunk"

    def __init__(self, make, model, year, battery_size="57"):
        '''
        Inherits battery as an attribute
        '''
        self.make = make
        self.model = model
        self.year = year
        self.battery = Battery(battery_size)

    def __str__(self):
        '''
        This is will return a string that will be used 
        '''
        return self.make + " " + self.model + ", " + str(self.year)

class Truck(Vehicle):
    storage = "bed"

    def __init__(self, make, model, year, truck_rating, battery_size="35mw"):
        super().__init__(make, model, year, battery_size)
        self.truck_rating = truck_rating

    def __str__(self):
        '''
        This method involves accessing a function from the super class and adding to it with string concatenation.
        '''
        return super().__str__() + ", Truck Rating:" +  str(self.truck_rating)

class Car(Vehicle):
    '''

    '''

    def __init__(self, make, model, year, battery_size, backseat_capacity):
        super().__init__(make, model, year, battery_size)
        self.backseat_capacity = backseat_capacity
        
        
    def __str__(self):
        '''
        This method involves accessing a function from the super class and adding to it with string concatenation.
        '''
        return super().__str__() + ", Backseat Capacity: " +  str(self.backseat_capacity)

class Monte_carlo(Car, Truck):
    '''
    Again, we pretend Monte Carlos are the ones with backseats and truck beds and trucks do not.
    '''
    storage = 'bed'
    
    def __init__(self, make, model, year, battery_size, backseat_capacity, truck_rating, style_points):
        Car.__init__(self, make, model, year, battery_size, backseat_capacity)
        Truck.__init__(self, model, year, truck_rating, battery_size)
        self.style_points = style_points
        
    def __str__(self):
        '''
        This method involves accessing a function from the super class and adding to it with string concatenation.
        '''
        return Car.__str__(self) + Truck.__str__(self)
        
        
# --------------------------------------------------------------------------------------- #


vehicle_1 = Vehicle("Cheveralet", "Cobalt", 2008)
truck_1 = Truck("Ford", "F150", 2005, 9.5)
car_1 = Car("Cheveralet", "Lumina", 1997, "100mw", 4)
monte_carlo_1 = Monte_carlo('Monte', 'Carlo', 2003, '54mw', 3, 9.9, 1000000) 

print('Standard storage of vehcile class in vheicle_1: ' + vehicle_1.storage)
print('Standard storage of Truck class in truck_1: ' + truck_1.storage)

print('Vehicle __str__() method in vehicle_1: ' + vehicle_1.__str__())
print('Truck __str__() method in truck_1: ' + truck_1.__str__())
print('Car __str__() method in car_1: ' + car_1.__str__())
print('Monte_carlo __str__() method in monte_carlo_1: ' + monte_carlo_1.__str__())







