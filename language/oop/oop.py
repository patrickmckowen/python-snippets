'''
This is a basic demonstration of using classes.


'''

class Vehicle(object):
	'''

	'''
	# This is a class attribute. It is share among all instances.
	vehicle_count = 0

	# The init is an important step where the variables are set and functions are called upon creation.
	def __init__(self, name, salary):
		self.brand = brand
		self.manufacturing_cost = manufacturing_cost
		Employee.empCount += 1

	def displayCount(self):
		print("Total Vehicles %d" % Vehicle.vehicle_count)

	def displayVehicle(self):
                print("Brand: ", self.brand,  ", Manufacturing Cost: ", self.manufacturing_cost)
