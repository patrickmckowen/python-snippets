from classs import *

#----------------------------------------------------------------------------------------------------------------#

# Creating the class for testing
first_song = Sample('Sample Name', 'Song Name', 'Song Artist', 100, ['Light', 'Fun', 'Sleek'])

#----------------------------------------------------------------------------------------------------------------#

# Test 1 - Get count of instances of Sample() class.
def test_get_spl_count():
	assert first_song.get_spl_count() == 1

# Test 2-6 - Getting attributes
def test_get_spl_name():
	assert first_song.get_spl_name() == 'Sample Name'

def test_get_song_name():
	assert first_song.get_song_name() == 'Song Name'

def test_get_artist():
	assert first_song.get_artist() == 'Song Artist'

def test_get_spl_name():
	assert first_song.get_rating() == 100

def test_get_keywords():
	assert first_song.get_keywords() == ['Light', 'Fun', 'Sleek']

#----------------------------------------------------------------------------------------------------------------#

#Special Methods


'''
Go here to learn and copy them down: http://www.diveintopython3.net/special-method-names.html

'''
