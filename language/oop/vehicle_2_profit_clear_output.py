'''
Now we need to calculate the profit for the second car.

Lets use the same naming scheme for consistency.

and we know we ned that string conversion again.

'''


# First, the cost for manufacturing the vehicle
vehicle_2_manufacturing_cost = 9057.34

# Next the price charged for a customer buying this car
vehicle_2_sale_price = 17345.63

# With the manufacturing cost and sales price saved we can calculate profit
print('Car 2 Profit = ' + str(vehicle_2_sale_price - vehicle_2_manufacturing_cost))


