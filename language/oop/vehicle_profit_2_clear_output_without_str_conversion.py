'''
So we realized it would be clearer if we put explained what the output was.

So we looked it up and found that you can do this with strings.

'''


# First, the cost for manufacturing the vehicle
vehicle_1_manufacturing_cost = 14057.34

# Next the price charged for a customer buying this car
vehicle_1_sale_price = 27345.63

# With the manufacturing cost and sales price saved we can calculate profit
print('Car 1 Profit = ' + vehicle_1_sale_price - vehicle_1_manufacturing_cost)


