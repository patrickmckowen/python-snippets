'''



'''
import argparse


parser = argparse.ArgumentParser()

parser.add_argument("-l", "--language",
                    type=str,
                    choices=['python', 'ruby'],
                    help="What is the main programming language used for this project?")

args = parser.parse_args()
print(args.language)
