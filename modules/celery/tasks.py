from celery import Celery

app = Celery('tasks', broker='pyamqp://guest@localhost//',
             backend='amqp://guest@localhost//')

@app.task
def add(x, y):
    return x + y
