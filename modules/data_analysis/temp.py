'''
The following example shows a data ma, A, with two data sets,B and C:


'''
import pandas as pd

df1 = pd.DataFrame({'A': [0,0,0,0,0,1,1],
                    'B': [1,2,3,5,4,2,5],
                    'C': [5,3,4,1,1,2,3]})

a_group_desc = df1.groupby('A').describe()#.stack() # Uncomment stack for a diff view.

print(a_group_desc)
