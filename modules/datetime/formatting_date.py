# -*- coding: utf-8 -*-
"""
Created on Sat Sep 23 08:18:37 2017

@author: patri

Notes:
    Need to go more indepth about the 
    format() and str.format() 
    
    * it's suggested to learn format() first as it uses the "Format Specification Mini Language"
"""

# No, this is not an error
# there is a datetime object in the datetime module
## This is a good way to do the import 

#%%
from datetime import datetime

#%%

now = datetime.now()
#%% Brief format (09/23/2017 08:29:22)

now_fmt = format(now, '%m/%d/%Y %H:%M:%S')

#%% Longer format (Saturday September 23 2017 08:29:22)

now_fmt2 = format(now, '%A %B %d %Y %H:%M:%S')