from flask import Flask
import flask_restless
from sqlalchemy import Column, Date, DateTime, Float, Integer, Unicode, String
from sqlalchemy import ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import backref, relationship
from sqlalchemy.orm import scoped_session, sessionmaker

app = Flask(__name__)
engine = create_engine('sqlite:///testdb.db', convert_unicode=True)
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
mysession = scoped_session(Session)

Base = declarative_base()
Base.metadata.bind = engine


class Contact(Base):
    __tablename__ = 'contacts'
    contact_id = Column(Integer(), primary_key=True)
    f_name = Column(String(50))
    l_name = Column(String(50))
    company = Column(String(50))
    job_title = Column(String(50))
    email_address__home = Column(String(50))
    email_status__home = Column(String(50))
    email_permission_status__home = Column(String(50)) 
    email_address__other = Column(String(50))
    email_status__other = Column(String(50))
    email_permission_status__other = Column(String(50))
    email_address__work = Column(String(50))
    email_status__work = Column(String(50))
    email_permission_status__work = Column(String(50))
    phone__fax = Column(String(50))
    phone__home = Column(String(50))
    phone__mobile__1 = Column(String(50))
    phone__mobile__2 = Column(String(50))
    phone__work__1 = Column(String(50))
    phone__work__2 = Column(String(50))
    street_address_line_1__home = Column(String(50))
    city__home = Column(String(50))
    state_province__home = Column(String(50))
    zip_postal_code__home = Column(String(50))
    country_home = Column(String(50))
    street_address_line_1__home__2 = Column(String(50))
    state_province__home__2 = Column(String(50))
    country__home__2 = Column(String(50))
    city__other = Column(String(50))
    state_province__other_other = Column(String(50))
    street_address_line_1__work = Column(String(50))
    city_work = Column(String(50))
    state_province__work = Column(String(50))
    zip_postal_code__work = Column(String(50))
    country__work = Column(String(50))
    street_address_line_1__work__2 = Column(String(50))
    city_work__2 = Column(String(50))
    state_province__work__2 = Column(String(50))
    zip_postal_code__work__2 = Column(String(15))
    country__work__2 = Column(String(50))
    website = Column(String(50))
    custom_field__8 = Column(String(50))
    salutation = Column(String(50))
    home_address_line_1 = Column(String(50))
    home_city = Column(String(50))
    home_state = Column(String(50))
    home_zip_code = Column(String(15))
    primary_phone = Column(String(50))
    phone__2 = Column(String(50))
    phone__3 = Column(String(50))
    department = Column(String(50))
    custom_field__1 = Column(String(50))
    custom_field__2 = Column(String(50))
    custom_field__3 = Column(String(50))
    custom_field__4 = Column(String(50))
    custom_field__5 = Column(String(50))
    custom_field__6 = Column(String(50))
    custom_field__7 = Column(String(50))
    email_lists = Column(String(50))         # Important
    notes = Column(String(2500))
    source_name = Column(String(50))
    created_at = Column(String(50))
    updated_at = Column(String(50))
    
    def __init__(self, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q,
                 r, s, t, u, v, w, x, y, z, aa, bb, cc ,dd, ee, ff, gg,
                 hh, ii, jj, kk, ll, mm, nn, oo, pp, qq, rr, ss, tt, uu,
                 vv, ww, xx, yy, zz, aaa, bbb, ccc, ddd, eee, fff, ggg,
                 hhh, iii, jjj):
        self.__dict__.update({k: v for k, v in locals().items() if k != 'self'})
        '''self.f_name = a
        self.l_name = b
        self.company = c
        self.job_title = d
        self.email_address__home = e
        self.email_status__home = f
        self.email_permission_status__home = g
        self.email_address__other = h
        self.email_status__other = i
        self.email_permission_status__other = j
        self.email_address__work = k
        self.email_status__work = l
        self.email_permission_status__work = m
        self.phone__fax = n
        self.phone__home = o
        self.phone__mobile__1 = p
        self.phone__mobile__2 = q
        self.phone__work__1 = r
        self.phone__work__2 = s
        self.street_address_line_1__home = t
        self.city__home = u
        self.state_province__home = v
        self.zip_postal_code__home = w
        self.country_home = x
        self.street_address_line_1__home__2 = y 
        self.state_province__home__2 = z
        self.country__home__2 = aa
        self.city__other = bb
        self.state_province__other_other = cc
        self.street_address_line_1__work = dd
        self.city_work = ee
        self.state_province__work = ff
        self.zip_postal_code__work = gg
        self.country__work = hh
        self.street_address_line_1__work__2 = ii
        self.city_work__2 = jj
        self.state_province__work__2 = kk 
        self.zip_postal_code__work__2 = ll
        self.country__work__2 = mm
        self.website = nn
        self.custom_field__8 = oo 
        self.salutation = pp
        self.home_address_line_1 = qq 
        self.home_city = rr
        self.home_state = ss
        self.home_zip_code = tt
        self.primary_phone = uu
        self.phone__2 = vv
        self.phone__3 = ww
        self.department = xx
        self.custom_field__1 = yy
        self.custom_field__2 = zz
        self.custom_field__3 = aaa
        self.custom_field__4 = bbb
        self.custom_field__5 = ccc
        self.custom_field__6 = ddd
        self.custom_field__7 = eee
        self.email_lists = fff               # Important
        self.notes = ggg
        self.source_name = hhh
        self.created_at = iii
        self.updated_at = jjj'''
        
Base.metadata.create_all()

manager = flask_restless.APIManager(app, session=mysession)

contact_blueprint = manager.create_api(Contact,
                                      methods=['GET'])

app.register_blueprint(contact_blueprint)

app.run()
