import flask
import flask.ext.sqlalchemy
import flask.ext.restless

# Create the Flask application and the Flask-SQLAlchemy object.
app = flask.Flask(__name__)
app.config['DEBUG'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = flask.ext.sqlalchemy.SQLAlchemy(app)

# Create your Flask-SQLALchemy models as usual but with the following two
# (reasonable) restrictions:
#   1. They must have a primary key column of type sqlalchemy.Integer or
#      type sqlalchemy.Unicode.
#   2. They must have an __init__ method which accepts keyword arguments for
#      all columns (the constructor in flask.ext.sqlalchemy.SQLAlchemy.Model
#      supplies such a method, so you don't need to declare a new one).
'''
class Person(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode, unique=True)
    birth_date = db.Column(db.Date)


class Computer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode, unique=True)
    vendor = db.Column(db.Unicode)
    purchase_time = db.Column(db.DateTime)
    owner_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    owner = db.relationship('Person', backref=db.backref('computers',
                                                         lazy='dynamic'))
'''

class Contact(db.Model):
    __tablename__ = 'contacts'
    contact_id = db.Column(db.Integer(), primary_key=True)
    f_name = db.Column(db.String(50))
    l_name = db.Column(db.String(50))
    company = db.Column(db.String(50))
    job_title = db.Column(db.String(50))
    email_address__home = db.Column(db.String(50))
    email_status__home = db.Column(db.String(50))
    email_permission_status__home = db.Column(db.String(50)) 
    email_address__other = db.Column(db.String(50))
    email_status__other = db.Column(db.String(50))
    email_permission_status__other = db.Column(db.String(50))
    email_address__work = db.Column(db.String(50))
    email_status__work = db.Column(db.String(50))
    email_permission_status__work = db.Column(db.String(50))
    phone__fax = db.Column(db.String(50))
    phone__home = db.Column(db.String(50))
    phone__mobile__1 = db.Column(db.String(50))
    phone__mobile__2 = db.Column(db.String(50))
    phone__work__1 = db.Column(db.String(50))
    phone__work__2 = db.Column(db.String(50))
    street_address_line_1__home = db.Column(db.String(50))
    city__home = db.Column(db.String(50))
    state_province__home = db.Column(db.String(50))
    zip_postal_code__home = db.Column(db.String(50))
    country_home = db.Column(db.String(50))
    street_address_line_1__home__2 = db.Column(db.String(50))
    state_province__home__2 = db.Column(db.String(50))
    country__home__2 = db.Column(db.String(50))
    city__other = db.Column(db.String(50))
    state_province__other_other = db.Column(db.String(50))
    street_address_line_1__work = db.Column(db.String(50))
    city_work = db.Column(db.String(50))
    state_province__work = db.Column(db.String(50))
    zip_postal_code__work = db.Column(db.String(50))
    country__work = db.Column(db.String(50))
    street_address_line_1__work__2 = db.Column(db.String(50))
    city_work__2 = db.Column(db.String(50))
    state_province__work__2 = db.Column(db.String(50))
    zip_postal_code__work__2 = db.Column(db.String(15))
    country__work__2 = db.Column(db.String(50))
    website = db.Column(db.String(50))
    custom_field__8 = db.Column(db.String(50))
    salutation = db.Column(db.String(50))
    home_address_line_1 = db.Column(db.String(50))
    home_city = db.Column(db.String(50))
    home_state = db.Column(db.String(50))
    home_zip_code = db.Column(db.String(15))
    primary_phone = db.Column(db.String(50))
    phone__2 = db.Column(db.String(50))
    phone__3 = db.Column(db.String(50))
    department = db.Column(db.String(50))
    custom_field__1 = db.Column(db.String(50))
    custom_field__2 = db.Column(db.String(50))
    custom_field__3 = db.Column(db.String(50))
    custom_field__4 = db.Column(db.String(50))
    custom_field__5 = db.Column(db.String(50))
    custom_field__6 = db.Column(db.String(50))
    custom_field__7 = db.Column(db.String(50))
    email_lists = db.Column(db.String(50))         # Important
    notes = db.Column(db.String(2500))
    source_name = db.Column(db.String(50))
    created_at = db.Column(db.String(50))
    updated_at = db.Column(db.String(50))
    
    def __init__(self, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q,
                 r, s, t, u, v, w, x, y, z, aa, bb, cc ,dd, ee, ff, gg,
                 hh, ii, jj, kk, ll, mm, nn, oo, pp, qq, rr, ss, tt, uu,
                 vv, ww, xx, yy, zz, aaa, bbb, ccc, ddd, eee, fff, ggg,
                 hhh, iii, jjj):
        self.__dict__.update({k: v for k, v in locals().items() if k != 'self'})

# Create the database tables.
db.create_all()

# Create the Flask-Restless API manager.
manager = flask.ext.restless.APIManager(app, flask_sqlalchemy_db=db)

# Create API endpoints, which will be available at /api/<tablename> by
# default. Allowed HTTP methods can be specified as well.
#manager.create_api(Person, methods=['GET', 'POST', 'DELETE'])
#manager.create_api(Computer, methods=['GET'])
manager.create_api(Contact, methods=['GET'])

# start the flask loop
app.run()
