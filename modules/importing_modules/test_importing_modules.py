'''
Text at the top of the document
'''
# just Going to start by importing and getting the things from the __init__.py
from importing_modules import *

#
def test_addition():
    num1 = 4
    operator = '+'
    num2 = 3
    goal = 7
    assert get_starting_number(num1, operator, num2) == goal

def test_subtraction():
    num1 = 4
    operator = '-'
    num2 = 3
    goal = 1
    assert get_starting_number(num1, operator, num2) == goal

def test_multiplication():
    num1 = 4
    operator = 'x'
    num2 = 3
    goal = 12
    assert get_starting_number(num1, operator, num2) == goal

def test_division():
    num1 = 12
    operator = '%'
    num2 = 4
    goal = 3
    assert get_starting_number(num1, operator, num2) == goal
