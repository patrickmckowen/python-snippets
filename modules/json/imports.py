imports = {'shell' : ['subprocess'
                       ],

            'files_dirs' : ['os'
                            ],
                            
            'flask' : ['flask',
                       'flask-cors',
                       'flask-admin',
                       'flask-assets',
                       'flask-debugtoolbar',
                       'flask-markdown',
                       'flask-script',
                       'flask-security',
                       'flask-sqlalchemy',
                       'flask-wtf',
                       'jinja2',
                       'werkzeug'
                       ],
                       
            'apis': ['slackclient',
                     'soco'
                     ],
            
            'data_analysis': ['pandas',
                              'numpy',
                              'nummpydoc'
                              'matplotlib',
                              'nltk',
                              'seaborn',
                              'scipy',
                              'scikit-learn',
                              'scikit-image',
                              'prettytable'
                              ],
                              
            'utilities' : ['pickle',
                           'pickleshare',
                           'packaging',
                           'setuptools',
                           'distutils',
                           'pathlib2',
                           'path.py',
                           'virtualenv'
                           ],
                           
            'database' : ['sqlalchemy',
                          'pymysql',
                          'sqlite3'
                          ],
                          
            'parsing' : ['lmxl',
                         'json', 
                         'jsonschema',
                         'pyyaml',
                         'htmldom',
                         'markupsafe',
                         'html5lib'
                         ]}