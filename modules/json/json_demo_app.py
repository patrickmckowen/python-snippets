import json, os

# Create a variable to start storing our directory structure and file data
app = {}

# Now we'll start with the obligatory app.rb
app['app.rb'] = "Some routes and such..."

# We also need a direcory for our views
#  - It needs to be another dictionary since the files inside of it
#    will store file names as keys and their text as values.
app['views'] = {'layout.rb': 'Some shared code.'}

# Oh and we need an erb file for the index page.
app['views']['index.erb'] = 'Some code for the index page.'

# We, of course, need a 'public' directory as well
#  - There will actually be sub directories in this directory,
#    each with its own files.
#    So we will need to do as before and use dictionaries inside this one
app['public'] = {}
app['public']['css'] = {}
app['public']['js'] = {}
app['public']['images'] = {}

# And now we can store some file data in these dictionaries.
app['public']['css']['style.css'] = 'Some style code.'
app['public']['js']['app.js'] = 'Some js code.'
app['public']['images']['image_1.jpg'] = 'Description of the picture.'

# We'll now go ahead and print out the data structure to see what it looks like
#  - It's not a string so we can't 'concatenate' the prompt and the data structure
#    but can achieve the same thing by using a , instead of a +.
#  - We use \n\n to put some space between the prints.
#  - *As a side note, you can't write dictionaries to files without converting
#    them to strings first.
print('App data structure:\n\n', app, '\n\n\n')

# Encode the data structure and store it to a variable.
json_string = json.JSONEncoder().encode(app)

# Let's see if it worked.
print('Current json_string:\n\n' + json_string + '\n\n\n')

# Now let's write this to a file
with open('json_jsonencoder_encode_app.json', 'w') as file:
    file.write(json_string)

# We can also use the dump() function
# So we don't have to store it in a string first.
with open('json_dump_app.json', 'w') as file:
    json.dump(app, file)
    
# Now let's see if they worked. First the one first stored in a string.
with open('json_jsonencoder_encode_app.json', 'r') as file:
    print('When writing a json string to a file:\n\n' + file.read() + '\n\n\n')

# And using the dump() function which loads the json back into a python data structure.
with open('json_dump_app.json', 'r') as file:
    print('When using json.load:\n\n', json.load(file), '\n\n\n')

# We can now get rid of thos files
os.remove('json_jsonencoder_encode_app.json')
os.remove('json_dump_app.json')

# One more thing to demonstrate - We can give an option to make the json more readable.
#  - An indent of 1 uses 1 space for indetation, 2 uses 2 spaces for indentation etc.
#  - An 0 will use newlines but not indent.
readable_json = json.JSONEncoder(indent=4).encode(app)
print('Readable version of json String:\n\n' + readable_json)
