'''

Notes:
* Consistent with mathematical notation for intervals, a parenthesis means that the side is open
while the square bracket means that the side is closed
(inclusive). Which side is closed can be changed by passing right=False

Is .levels not a thing anymore?
'''
import nltk
from nltk.book import text4
#print(help(text4))

# Prompt
print('Using Inaugural Address', end="\n\n")

#
print('Search and display word in context')
print(text4.concordance("vote"), end="\n\n")

print('Find  words that appear in similar contexs')
print(text4.similar('vote'), end="\n\n")

print('Collations - Words that appear together frequently')
print(text4.collocations(), end="\n\n")


# Number of words
print('The total number of words')
print(len(text4), end="\n\n")

print('Number of distinct words')
print(len(set(text4)), end="\n\n")

print('Richness of the text')
print(len(text4) / len(set(text4)))
print(100 * text4.count('democracy') / len(text4), end="\n\n")

print('Generating text in the style of the inagural address')
print(help(nltk.text.Text))
