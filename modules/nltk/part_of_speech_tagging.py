'''
grouping words into classes such as verbs and adjectives

Vocab:
- Tagset - The collection of tags for a particular task
- Backoff - Starting with a more specialized tagger (like a bigram)
  then moving to a more generalized one (like unigram) if it can't
  tag a word in a particular context.
Synonym: Parts of speech - word classes, lexical categories

Nltk can do automatic tagging

Why is it important?
- Predicting the behavior of previously unseeen words
- Analyzing word usage in corpora
- Text to speech speech systems
- classification

Several types of taggers
- default
- regular expression
- unigram
- n-gram

Taggers can be trained and evaluated using tagged corpora

'''
import nltk


# Prompt
print('Part of speech tagging', end="\n\n")

print('Some demo tag words from nltk.corpus.brown.tagged_words()')
print(nltk.corpus.brown.tagged_words())
