'''


 - append()
 - concat()
 - merge()
 - join()


concat just ads to the bottom and doesn't do it too intuitively

append - There will be no not a number 
   - more 

can't give initialized df the function".concat()"
   - Need to use append.
'''
import pandas as pd


#
df = pd.DataFrame([[1, 2], [3, 4]], columns=list('AB'))
df2 = pd.DataFrame([[5, 6], [7, 8]], columns=list('AB'))


#%%
print(df)

#%% NOT GOING TO SHOW THE APPENDED ROW
df.append(df2)
print(df)

#%% SHOW APPENDED ROW
print(df.append(df2))

#%%PERMANENTLY insert ROW
print(df.append(df2), inplace=True)
print(df)

#%% IGNORE INDEX: Makes the new rows enxtend current df's index
#df.append(df2, ignore_index=True)

# 3 sets of data with four values
df1 = pd.DataFrame({'A': ['A0', 'A1', 'A2', 'A3'],
                     'B': ['B0', 'B1', 'B2', 'B3'],
                     'C': ['C0', 'C1', 'C2', 'C3'],
                     'D': ['D0', 'D1', 'D2', 'D3']})

df2 = pd.DataFrame({'A': ['A4', 'A5', 'A6', 'A7'],
                     'B': ['B4', 'B5', 'B6', 'B7'],
                     'C': ['C4', 'C5', 'C6', 'C7'],
                     'D': ['D4', 'D5', 'D6', 'D7']})

df3 = pd.DataFrame({'A': ['A8', 'A9', 'A10', 'A11'],
                     'B': ['B8', 'B9', 'B10', 'B11'],
                     'C': ['C8', 'C9', 'C10', 'C11'],
                     'D': ['D8', 'D9', 'D10', 'D11']})

# Here is a single row. The items need to be in a list format to avoid
# an error telling you you need to pass and index for scalar items.
df4 = pd.DataFrame({'A': ['A12'],
                     'B': ['B12'],
                     'C': ['C12'],
                     'D': ['D11']})

# We group the first threee data frames together 
frames = [df1, df2, df3]

# If i don't put "ignore_index=True" there would not be a single continuous index
# We pass the list variable that contains the 3=first 3 dataframes ('frames')
result = pd.concat(frames, ignore_index=True)

print(result)
print("\n\n")

# APPEND()
# Use ignore_index=True for this one too.
result2 = result.append(df4, ignore_index=True)
print(result2)

result2['sum'] = result2.sum(axis=1)

print("\n\n")

print(result2)

# RENAME COLUMN
result2.rename(columns = {'D' : 'Z'}, inplace = True)

print("\n\n")

print(result2)
