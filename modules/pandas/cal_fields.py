# -*- coding: utf-8 -*-
"""
Created on Sat Jul  8 17:17:02 2017

@author: Patrick
"""

#%% IMPORTS
import pandas as pd

#%%
data = [['Alex',10],['Bob',12],['Clarke',13]]
df = pd.DataFrame(data,columns=['Name','Age'],dtype=float)
print(df)

#%%

import pandas as pd
data = [['Alex',10],['Bob',12],['Clarke',13]]
df = pd.DataFrame(data,columns=['Name','Age'])
print(df)

#%%

import pandas as pd

data = [
        ['Ganandorf', '100', 'Keeping it real is critical.'],
        ['Warf', '101', 'Today is a good day to die.']
       ]

df = pd.DataFrame(data, columns=['Name', 'Age', 'Sayings'])

print(df)
