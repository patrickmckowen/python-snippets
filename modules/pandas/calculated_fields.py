'''



'''
import pandas as pd

# -------------------------------------------------------------------- #

# 3 sets of data with four values
df1 = pd.DataFrame({'Name': ['Steve', 'Sandy', 'Paul', 'Patty'],
                     'Age': [23, 34, 29, 36],
                     'Pounds': [45, 76, 98, 150],
                     'Inches': [150, 145, 130, 137]}, index=['Steve', 'Sandy', 'Paul', 'Patty'])

df2 = pd.DataFrame({'Name': ['Jane', 'Eric', 'Betty', 'Kyle'],
                     'Age': ['B4', 'B5', 'B6', 'B7'],
                     'Pounds': ['C4', 'C5', 'C6', 'C7'],
                     'Inches': ['D4', 'D5', 'D6', 'D7']})

df3 = pd.DataFrame({'Name': ['Andy', 'Susan', 'Andy', 'Carl'],
                     'Age': ['B8', 'B9', 'B10', 'B11'],
                     'Pounds': ['C8', 'C9', 'C10', 'C11'],
                     'Inches': ['D8', 'D9', 'D10', 'D11']})

# -------------------------------------------------------------------- #

frames = [df1, df2, df3]

result = pd.concat(frames, ignore_index=True)

# -------------------------------------------------------------------- #

# This is weird because I'm using strings instead of numbers but they
# do as expected.
df1['sum'] = df1.sum(axis=1)

print(result)
