'''

Notes:
* Consistent with mathematical notation for intervals, a parenthesis means that the side is open
while the square bracket means that the side is closed
(inclusive). Which side is closed can be changed by passing right=False

Is .levels not a thing anymore?
'''
import pandas as pd
import numpy as np

ages = [20, 22, 25, 27, 21, 23, 37,31, 61, 61, 45, 41, 32]

bins = [18, 25, 60, 100]

print('Lets divide thes age range into bins of 18-25, 26-35,,36-60,61-100')

cats = pd.cut(ages, bins)

print("Cats variable value after using pd.cut(ages, bins)")
print(cats, end="\n\n")

# The object pandas returns is a special categorical variable
# You can treat it like array of strings
# Internall it contains a levels array indicating the distinct category names
# along with a labeling for the ages data in the labels atribute

print('Cats.codes')
print(cats.codes, end="\n\n")



print('Get the count of each category ("pd.value_counts(cats)")')
print(pd.value_counts(cats), end="\n\n")

cats2 = pd.cut(ages, bins)

print('cats2')
print(cats2, end="\n\n")
print(cats2.codes, end="\n\n")

# You can also pass explicit bin names
group_names = ['Youth', 'YoungAdult', 'MiddleAged', 'Senior']

#
cats3 = pd.cut(ages, [18, 26, 36, 61, 100], labels=group_names)

#
print(cats3, end="\n\n")

#
print('cats3.codes')
print(cats3.codes, end="\n\n")

#
print('dir(cats3)')
print(dir(cats3.labels), end="\n\n")

# If you pass cut an integer of bins instead of explicit bin edges, it will split them evenly

data = np.random.rand(20)

print('Splitting the groups even into 4 segments')
print(pd.cut(data, 4, precision=2), end="\n\n")


# Working with quartiles
data = np.random.randn(1000) # normally distributed

# Cut into quartiles
cats4 = pd.cut(data, 4)

print("Quartiles with qcut")
print(cats4, end="\n\n")
