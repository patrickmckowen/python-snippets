import pandas as pd

data = pd.DataFrame({'k1':['one'] * 3 + ['two'] * 4,
                     'k2':[1, 1, 2, 3, 3, 4, 4]})

print("Origional dataset")
print(data, end="\n\n")

print("Duplicated Datasets")
print(data.duplicated(), end="\n\n")

print("Dropping duplicates on all rows")
print(data.drop_duplicates(), end="\n\n")


# Can also drop subsets
# Imagine we added an extra column and wanted to
# Filter duplicates only based on the k1 column
data['v1'] = range(7)

print("With extra row added.")
print(data, end="\n\n")

print("Filtering on k1 column duplicates")
print(data.drop_duplicates(['k1']), end="\n\n")
