# -*- coding: utf-8 -*-
"""
Created on Tue Sep  5 16:24:19 2017

@author: pi
"""

df3 = pd.DataFrame({'Name': ['Jane', 'Eric', 'Betty', 'Kyle'],
                     'Age': [1, 2, 3, 4],
                     'Pounds': [7, 6, 5, 4],
                     'Inches': ['D4', 'D5', 'D6', 'D7']})
                     
# Assigning to new variable | Not specifying column
#   - Without 'inplace=True', it just returns a copy
#   - Without specifying a column it goes over all columns
df4 = df3.replace([1,2,3,4,5,6,7],[7,6,5,4,3,2,1])


df3['REAge'] = df3['Age'].replace([1,2,3,4,5,6,7],[7,6,5,4,3,2,1])

DOES NOT WORK
#df3['REAge']['Pounds'].replace([1,2,3,4,5,6,7],[7,6,5,4,3,2,1])
# Can do in place
#df3.replace([1,2,3,4,5,6,7],[7,6,5,4,3,2,1], inplace=True)

# This should work but it kept saying there was an overlap
# df3.replace({'Age':{1:7,2:6,3:5,5:3}})