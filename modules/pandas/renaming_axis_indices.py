'''
*Replacing using dictionaries, automatically replaces in place
* BUt not when just passing in values

'''
import pandas as pd
import numpy as np
data = pd.DataFrame(np.arange(12).reshape((3,4)),
                    index=['Ohio', 'Colorado', 'New York'],
                    columns=['one','two','three', 'four'])

print("Origional dataset")
print(data, end="\n\n")


# Using .map
print('You can use map on these now to')
print(data.index.map(str.upper), end="\n\n")

print('The origional variable is unchanged')
print(data, end="\n\n")

# Set it inplace
data.index = data.index.map(str.upper)

print('')
print(data, end="\n\n")

# Use rename to modify a dataframe
print('Using rename to return a reference with axes indices replaced')
print(data.rename(index=str.title, columns=str.upper), end="\n\n")

print('And its back again')
print(data, end="\n\n") # Show that it was only a reference

# Using dict-like objects
print('Renaming using dicts')
print(data.rename(index={'OHIO':'Indiana'},
            columns={'three':'pekaboo'}), end="\n\n")

print('And its back again')
print(data, end="\n\n") # Show that it was only a reference

# Using inplace=True
print('After using "inplace=True" and printing origional variable value')
data.rename(index={'OHIO':'Indiana'},
            columns={'three':'pekaboo'}, inplace=True)
print(data, end="\n\n") # Show that it was only a reference

# What's the _ = about?
data.rename(index={'OHIO':'Indiana'},
            columns={'pekaboo':'sqweebledable'}, inplace=True)

print('Testing what the _ means. It means nothing. You don't have ot use it.')
print(data, end="\n\n")
