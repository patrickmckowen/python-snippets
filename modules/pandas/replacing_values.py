'''
*Replacing using dictionaries, automatically replaces in place
* BUt not when just passing in values

'''
import pandas as pd
import numpy as np
data = pd.DataFrame({'name':['P-Money', 'Steve', 'Paul'],
                     'number':[1, 2, 3]})

print("Origional dataset")
print(data, end="\n\n")

# Nicknames mapping
nicknames = {"P-Money":"P-$", 'Steve':'Steveyboy', 'Paul':'Pall'}

# NOTE: Using replace with a dict automatically replaces in-place
data.replace(nicknames)

print("Replacing names with nicknames from dict")
print(data, end="\n\n")

print('Replacement reference (i.e. not "in-place")')
print(data.replace(1, 9), end="\n\n")

print('Print the data set again and notice it is back to the way it was.')
print(data, end="\n\n")

# Use inplace=True
data.replace(1, 9, inplace=True)

print('Using "data.replace(1, 9, inplace=True)" and referencing the origional variable value')
print(data, end="\n\n")

print(data.replace([2,3], 1))

print(data.replace([2,3], [5, 6]))

print(data.replace([2,3], range(2)))


# Also, don't forget to make sure non-ish variables are converted to the standard np.nan so pandas can work with them
print(data.replace(9, np.nan))


