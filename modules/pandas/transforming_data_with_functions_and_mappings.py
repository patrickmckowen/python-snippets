import pandas as pd

data = pd.DataFrame({'name':['P-Money', 'Steve', 'Paul'],
                     'number':[1, 2, 3]})

print("Origional dataset")
print(data, end="\n\n")

# Nicknames mapping
nicknames = {"P-Money":"P-$", 'Steve':'Steveyboy', 'Paul':'Pall'}

# Add a nicknames column
data['nickname'] = data['name'].map(nicknames)
print("Added a nickname column")
print(data, end="\n\n")

print("Duplicated Datasets")
print(data.duplicated(), end="\n\n")

print("Dropping duplicates on all rows")
print(data.drop_duplicates(), end="\n\n")

# You can also do it with a function
data['name_capitalized'] = data['name'].map(lambda x: x.upper())

print("Adding a column for names capitalized.")
print(data, end="\n\n")
