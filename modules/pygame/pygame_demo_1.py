import pygame
from pygame.locals import *
screen_mode = (640, 480)
color_black = 0,0,0
class Game:
	# this gets called first
	def __init__(self):
		pygame.init()
		self.screen = pygame.display.set_mode(screen_mode)
		pygame.display.set_caption("PyGame intro")
		self.quit = False
	# put game update code here
	def update(self):
		return
	# put drawing code here
	def draw(self):
		self.screen.fill(color_black)
		pygame.display.flip()

	# the main game loop
	def mainLoop(self):
		while not self.quit:
			# handle events
			for event in pygame.event.get():
				if event.type == QUIT:
					self.quit = True
					self.update()
					self.draw()
if __name__ == '__main__' :
	game = Game()
	game.mainLoop()
