import pygame
from pygame.locals import *
screen_mode = (640, 480)
color_black = 0,0,0
class Game:
	# this gets called first
	def __init__(self):
		pygame.init()
		self.screen = pygame.display.set_mode(screen_mode, pygame.FULLSCREEN, pygame.NOFRAME)
		pygame.display.set_caption("PyGame intro")
		self.quit = False
		self.image = pygame.image.load("/home/patrick/Desktop/HeadFirstPHPMySQL_code_all (1)/examples/ch12/final/aliens/fang.jpg")
		self.x = 320
		self.y = 240
		self.clock = pygame.time.Clock()
	# put game update code here
	def update(self):
		keys = pygame.key.get_pressed()
		if keys[K_LEFT]:
			self.x = self.x - 1
		if keys[K_RIGHT]:
			self.x = self.x + 1
			#(self.x, self.y) = pygame.mouse.get_pos()
		return
	# put drawing code here
	def draw(self):
		# clear the screen
		self.screen.fill(color_black)
		# add code here
		self.screen.blit(self.image, (self.x, self.y))
		# display updated scene
		pygame.display.flip()
	# the main game loop
	def mainLoop(self):
		while not self.quit:
			# handle events
			for event in pygame.event.get():
				if event.type == QUIT:
					self.quit = True
			self.update()
			self.draw()
			self.clock.tick(60)
		
if __name__ == '__main__' :
	game = Game()
	game.mainLoop()
