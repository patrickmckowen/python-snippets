#%%
import os, sqlite3

#%%
from datetime import datetime

#%%
conn = sqlite3.connect('login.db')

#%%
c = conn.cursor()

#%%
def get_date():
    return format(datetime.now(), '%m/%d/%Y %H:%M:%S')

#%%
def create_table_users():
    c.execute("""CREATE TABLE users (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username TEXT,
        password TEXT,
        join_date TEXT
        );""")
    
#%%
def create_table_messages():
    c.execute("""CREATE TABLE IF NOT EXISTS uploads (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        message TEXT,
        date_time TEXT,
        sender_id INTEGER,
        reciever_id INTEGER,
        FOREIGN KEY (sender_id) REFERENCES users (id)
        FOREIGN KEY (reciever_id) REFERENCES users (id)
        );""")

def create_tables():
    create_table_users()
    create_table_messages()
    

#
def check_if_user_exists(username):
    return c.execute("""SELECT * FROM users WHERE username='{}'""".format(username))
    
# Insert user
def insert_user(username, password):
    c.execute("""INSERT INTO users (username, password, join_date) VALUES (?, ?, ?)""",
              (username, password, get_date())) 

# Lookup user

# Update user info

# Delete user
