# -*- coding: utf-8 -*-
"""
Created on Sat Sep 23 05:55:02 2017

@author: patri
"""

#%%
import os, sqlite3

#%%
from datetime import datetime

#%%
conn = sqlite3.connect('test.db')

#%%
c = conn.cursor()

#%%
def create_table_uploads():
    c.execute("""CREATE TABLE IF NOT EXISTS uploads (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        upload_name TEXT,
        upload_description TEXT,
        upload_date TEXT
        );""")
    
#%%
def create_table_audio():
    c.execute("""CREATE TABLE IF NOT EXISTS audio (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        file_name TEXT,
        file_type TEXT,
        upload_id INTEGER,
        FOREIGN KEY (upload_id) REFERENCES upload (id)
        );""")

#%%
def get_date():
    return format(datetime.now(), '%m/%d/%Y %H:%M:%S')

#%%
def upload(upload_name, upload_description, file_name, file_type):
    c.execute("INSERT INTO uploads (upload_name, upload_description, upload_date) VALUES(?, ?, ?)", (upload_name, upload_description, get_date()))
    upload_id = c.lastrowid
    c.execute("INSERT INTO audio (file_name, file_type, upload_id)
              VALUES (?, ?, ?)", (file_name, file_type, upload_id))
    conn.commit()

#%%
def setup_db():
    create_table_uploads()
    create_table_audio()

#%%
def main():
    setup_db()
    upload('Saturday Upload', 'Just Backing up some files', 'drum1.WAV', '.WAV')

#%%
if __name__ == '__main__':
    main()
