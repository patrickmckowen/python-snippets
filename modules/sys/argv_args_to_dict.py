import sys, json

def args_to_dict():
    dictionary = {}
    for i in sys.argv[1:]:
        if i.startswith("--"):
            dictionary[i[2:]] = sys.argv[sys.argv.index(i)+1]
    return dictionary

args = args_to_dict()

print(args)


