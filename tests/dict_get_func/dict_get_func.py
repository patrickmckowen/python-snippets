# -*- coding: utf-8 -*-
"""
Created on Fri Jul  7 22:32:10 2017

@author: Patrick
"""

#%% declare dictionary

dikt = {'One':'1', 'two':'2', 'three':'3'}

#%% .get() an existing item

dikt.get('three', 'Shit aint here, son.')

#%% .get() a nonexistant item

dikt.get('four', 'Shit aint here, son.')
