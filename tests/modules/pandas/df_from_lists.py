import pandas as pd

data = [
        ['Ganandorf', '100', 'Keeping it real is critical.'],
        ['Warf', '101', 'Today is a good day to die.']
       ]

df = pd.DataFrame(data, columns=['Name', 'Age', 'Sayings'])


print(df)
